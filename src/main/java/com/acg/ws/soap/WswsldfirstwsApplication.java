package com.acg.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WswsldfirstwsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WswsldfirstwsApplication.class, args);
	}

}
